import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFRequestReadOnly;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class ExistsFileAccessor extends StandardAccessorImpl {

    @Override
    public void onSource(INKFRequestContext context) throws Exception {
        String filePath = context.source("httpRequest:/queryparam/filePath", String.class);

        INKFRequest request = context.createRequest("file:///" + filePath);
        request.setVerb(INKFRequestReadOnly.VERB_EXISTS);

        Boolean exists = (Boolean) context.issueRequest(request);

        INKFResponse response = context.createResponseFrom(String.format("File %s : %s",
                filePath,
                (exists ? "exists" : "does not exist")));
        response.setMimeType("text/plain");
    }
}

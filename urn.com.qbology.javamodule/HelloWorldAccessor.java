import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class HelloWorldAccessor extends StandardAccessorImpl {

    @Override
    public void onSource(INKFRequestContext context) throws Exception {
        String url = context.source("httpRequest:/url", String.class);
        String method = context.source("httpRequest:/method", String.class);
        IHDSNode node = (IHDSNode) context.source("httpRequest:/params");
        String pageNum = context.source("httpRequest:/queryparam/pageNum", String.class);
        String result = "Hello Servlet World" +
                "\nRequest Identifier = " + context.getThisRequest().getIdentifier() +
                "\nHTTP Method = " + method +
                "\nNode = " + node.toString() +
                "\nPage num = " + pageNum +
                "\nHTTP URL = " + url;

        List<String> names = new ArrayList<>();
        names.add("Krakow2");
        names.add("Pcim2");
        names.add("Kcim2");

        names.stream().forEach(new Consumer<String>() {
            @Override
            public void accept(String name) {
                System.out.println(name);
            }
        });

        context.createResponseFrom(result);
        context.sink("httpResponse:/redirect", "http://www.onet.pl");
    }
}

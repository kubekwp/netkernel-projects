CREATE TABLE IF NOT exists investment (
    id        	INTEGER IDENTITY,
    name 		VARCHAR(255),
    amount    	INTEGER,
    percent	   	INTEGER,
    months		INTEGER
);

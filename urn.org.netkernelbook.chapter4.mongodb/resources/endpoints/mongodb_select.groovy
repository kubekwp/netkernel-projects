import com.mongodb.*;
import com.mongodb.util.*
import org.json.*;

jsonResult = new JSONObject();
jsonResult.put("success", true);
jsonResult.put("errors", []);
jsonResult.put("message", '');

databaseserver = context.source("arg:databaseserver", String.class);
databasename = context.source("arg:databasename", String.class);
collectionname = context.source("arg:collectionname", String.class);

criteria = null;

try {
    criteria = context.source("arg:criteria", JSONObject.class);
} catch (ec) {
    jsonResult.putOpt("error", ec);
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument criteria is missing or incorrect");
}

fields = null;

try {
    fields = context.source("arg:fields", JSONObject.class);
} catch (ef) {
    jsonResult.putOpt("error", ef);
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument fields is missing or incorrect");
}

optskip = null;

try {
    optskip = context.source("arg:skip", Integer.class);
} catch (es) {
    optskip = 0;
}

optlimit = null;

try {
    optlimit = context.source("arg:limit", Integer.class);
} catch (el) {
    optlimit = 0;
}

optsort = null;

try {
    optsort = context.source("arg:sort", JSONObject.class);
} catch (es) {
    optsort = new JSONObject();
}

selectresult = null;

if ((criteria != null) && (fields != null)) {
    try {
        mConnection = new Mongo(databaseserver);
        mDatabase = mConnection.getDB(databasename);
        mCollection = mDatabase.getCollection(collectionname);

        selectresult = new JSONArray();

        selectcursor = mCollection.find(
                JSON.parse(criteria.toString()),
                JSON.parse(objNew.toString()))
                .sort(JSON.parse(optsort.toString())).skip(optskip).limit(optlimit);

        selectcursor.each{
            selectresult.put(new JSONObject(it.toString()));
        };

        mConnection.close();
    } catch (e) {
        if (mConnection) {
            mConnection.close();
        }
        jsonResult.putOpt("success", false);
        jsonResult.putOpt("message", "Select failed on Mongo database");
    }
}

if (selectresult != null) {
    response = context.createResponseFrom(selectresult);
} else {
    response = context.createResponseFrom(jsonResult);
}
response.setMimeType('application/json');
response.setExpiry(response.EXPIRY_ALWAYS);
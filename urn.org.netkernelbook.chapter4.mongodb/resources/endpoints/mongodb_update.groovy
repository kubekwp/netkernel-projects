import com.mongodb.*;
import com.mongodb.util.*
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.json.*;

jsonResult = new JSONObject();
jsonResult.put("success", true);
jsonResult.put("errors", []);
jsonResult.put("message", '');

databaseserver = context.source("arg:databaseserver", String.class);
databasename = context.source("arg:databasename", String.class);
collectionname = context.source("arg:collectionname", String.class);

criteria = null;

try {
    criteria = context.source("arg:criteria", JSONObject.class);
} catch (ec) {
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument criteria is missing or incorrect");
}

objNew = null;

try {
    objNew = context.source("arg:objNew", JSONObject.class);
} catch (eo) {
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument objNew is missing or incorrect");
}

upsert = null;

try {
    upsert = context.source("arg:upsert", Boolean.class);
} catch (eu) {
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument upsert is missing or incorrect");
}

multi = null;

try {
    multi = context.source("arg:multi", Boolean.class);
} catch (em) {
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument multi is missing or incorrect");
}

if ((criteria != null) && (objNew != null) && (upsert != null) && (multi != null)) {
    try {
        mConnection = new Mongo(databaseserver);
        mDatabase = mConnection.getDB(databasename);
        mCollection = mDatabase.getCollection(collectionname);

        updateresult = mCollection.update(
                JSON.parse(criteria.toString()),
                JSON.parse(objNew.toString()),
                upsert,
                multi);

        if (updateresult.getLastError().ok()) {
            if (updateresult.getError()) {
                jsonResult.putOpt("success", false);
                jsonResult.putOpt("message", updateresult.getError());
            } else {
                jsonResult.putOpt("message", "update is successful");
            }
        } else {
            jsonResult.putOpt("success", false);
            jsonResult.putOpt("message", updateresult.getLastError().getErrorMessage());
        }
        mConnection.close();
    } catch (e) {
        if (mConnection) {
            mConnection.close();
        }
        jsonResult.putOpt("success", false);
        jsonResult.putOpt("message", "Update failed on Mongo database");
    }
}

response = context.createResponseFrom(jsonResult);
response.setMimeType('application/json');
response.setExpiry(response.EXPIRY_ALWAYS);
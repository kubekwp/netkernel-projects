import com.mongodb.*;
import com.mongodb.util.*;
import org.json.*;

jsonResult = new JSONObject();
jsonResult.put("success", true);
jsonResult.put("errors", []);
jsonResult.put("message", '');

databaseserver = context.source("arg:databaseserver", String.class);
databasename = context.source("arg:databasename", String.class);
collectionname = context.source("arg:collectionname", String.class);

try {
    mConnection = new Mongo(databaseserver);
    mDatabase = mConnection.getDB(databasename);
    mCollection = mDatabase.getCollection(collectionname);

    mCollection.drop();

    jsonResult.putOpt("message", "Drop is successfull");

    mConnection.close();
} catch (e) {
    if (mConnection) {
        mConnection.close();
    }
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Drop failed on Mongo database");
}

response = context.createResponseFrom(jsonResult);
response.setMimeType('application/json');
response.setExpiry(response.EXPIRY_ALWAYS);
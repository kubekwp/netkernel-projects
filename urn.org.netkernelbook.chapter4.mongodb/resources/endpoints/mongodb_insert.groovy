import com.mongodb.*;
import com.mongodb.util.*;
import org.json.*;

jsonResult = new JSONObject();
jsonResult.put("success", true);
jsonResult.put("errors", []);
jsonResult.put("message", '');

databaseserver = context.source("arg:databaseserver", String.class);
databasename = context.source("arg:databasename", String.class);
collectionname = context.source("arg:collectionname", String.class);

docs = null;

try {
    docs = context.source("arg:docs", JSONObject.class);
} catch (eo) {
    jsonResult.putOpt("errors", eo);
    try {
        docs = context.source("arg:docs", JSONArray.class);
    } catch (ea) {
        jsonResult.putOpt("success", false);
        jsonResult.putOpt("message", "Argument docs is missing or incorrect");
    }
}

if ((docs != null)) {
    try {
        mConnection = new Mongo(databaseserver);
        mDatabase = mConnection.getDB(databasename);
        mCollection = mDatabase.getCollection(collectionname);
        insertresult = mCollection.insert(JSON.parse(docs.toString()));

        jsonResult.putOpt("ack", insertresult.wasAcknowledged());
        jsonResult.putOpt("message", "insert is successful");

        mConnection.close();
    } catch (e) {
        if (mConnection) {
            mConnection.close();
        }
        jsonResult.putOpt("success", false);
        jsonResult.putOpt("errors", e);
        jsonResult.putOpt("message", "Insert failed on Mongo database");
    }
}

response = context.createResponseFrom(jsonResult);
response.setMimeType('application/json');
response.setExpiry(response.EXPIRY_ALWAYS);
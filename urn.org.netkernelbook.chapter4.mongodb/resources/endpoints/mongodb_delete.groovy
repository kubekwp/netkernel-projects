import com.mongodb.*;
import com.mongodb.util.*;
import org.json.*;

jsonResult = new JSONObject();
jsonResult.put("success", true);
jsonResult.put("errors", []);
jsonResult.put("message", '');

databaseserver = context.source("arg:databaseserver", String.class);
databasename = context.source("arg:databasename", String.class);
collectionname = context.source("arg:collectionname", String.class);

criteria = null;

try {
    criteria = context.source("arg:criteria", JSONObject.class);
} catch (ec) {
    jsonResult.putOpt("success", false);
    jsonResult.putOpt("message", "Argument criteria is missing or incorrect");
}

if ((criteria != null)) {
    try {
        mConnection = new Mongo(databaseserver);
        mDatabase = mConnection.getDB(databasename);
        mCollection = mDatabase.getCollection(collectionname);

        deleteresult = mCollection.remove(JSON.parse(criteria.toString()));

        if (deleteresult.getLastError().ok()) {
            if (deleteresult.getError()) {
                jsonResult.putOpt("success", false);
                jsonResult.putOpt("message", deleteresult.getError());
            } else {
                jsonResult.putOpt("message", "delete is successful");
            }
        } else {
            jsonResult.putOpt("success", false);
            jsonResult.putOpt("message", deleteresult.getLastError().getErrorMessage());
        }
        mConnection.close();
    } catch (e) {
        if (mConnection) {
            mConnection.close();
        }
        jsonResult.putOpt("success", false);
        jsonResult.putOpt("message", "Delete failed on Mongo database");
    }
}

response = context.createResponseFrom(jsonResult);
response.setMimeType('application/json');
response.setExpiry(response.EXPIRY_ALWAYS);
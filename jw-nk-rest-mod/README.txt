Installing and running the examples.

This module is intended to go with Brian Sletten's JavaWorld article on NetKernel.

To run the examples, please download NetKernel 3.3.1 here:

http://www.1060.org/download/

Once you retrieve the jar, run:

java -jar 1060-NetKernel-SE-DK-3.3.1.jar

This will allow you to pick a location in which to install NetKernel (we refer to this
directory below as <NK-Install>). Once installed, go to that directory and unzip (or move) 
this Zip file into the modules directory. Make sure that there is a directory for the 
module in the modules directory. It will be created by hand if you re-unzip this zip there, 
otherwise create it by hand and copy the content of this zip into it.

After this, you should see this kind of structure under the NetKernel installation:

HarryHood:1060-NKSEDK-3.3.1 brian$ ls modules/jw-nk-rest-mod/

README.txt	data		etc		module.xml	scripts		xunit
content		entrypoints.xml	links.xml	pub		xml

Now, edit the <NK-Install>/etc/deployedModules.xml file to include this new module
by adding the following line:

<module>modules/jw-nk-rest-mod/</module>

If you hand-created a name other than "jw-nk-rest-mod", use that.

You need to make sure to expose the functionality from this module through NetKernel's 
front end fulcrum (so you can hit it via HTTP). Add the following to the
<NK-Install>/modules/mod-fulcrum-frontend/module.xml:

<import>
    <uri>urn:net:bosatsu:jw-rest</uri>
</import>

You can put it right below the section that says:

<!--
************
Add your modules below here...
************
-->

Now you can start NetKernel with either:

bin/start.sh ; Unix

or

bin/startup.bat ; Windows

Note, NetKernel uses port 8080 by default. If this conflicts with something else running
on your machine, you can edit <NK-Install>/mod-fulcrum-frontend/etc/TransportJettyConfig.xml.
Search for "8080" and pick another port. This will take affect the next time NetKernel is
restarted. If you change the port, make sure to edit the URL below to test.

You should be able to hit:

http://localhost:1060/ep+name@app_xunit

Execute the Customer unit test and see a green bar. Follow along from the article for what else is going on.
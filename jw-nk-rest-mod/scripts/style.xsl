<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    
    <xsl:template match="customers">
        <table class="customer">
            <tr class="header">
                <th>ID</th>
                <th>Name</th>
                <th>Street</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
            </tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    
    <xsl:template match="customer">
        <tr>
            <xsl:if test="position() mod 2 != 0">
              <xsl:attribute  name="class">d0</xsl:attribute>
            </xsl:if>            
            <xsl:if test="position() mod 2 != 1">
              <xsl:attribute  name="class">d1</xsl:attribute>
            </xsl:if>
            <td><xsl:value-of select="@id"/></td>
            <td><xsl:value-of select="name"/></td>
            <td><xsl:value-of select="address/street"/></td>
            <td><xsl:value-of select="address/city"/></td>
            <td><xsl:value-of select="address/state"/></td>
            <td><xsl:value-of select="address/zipcode"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>

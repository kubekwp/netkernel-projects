<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    
    <xsl:template match="results">
        <customers>
            <xsl:apply-templates/>
        </customers>
    </xsl:template>
    
    <xsl:template match="row">
        <customer>
            <xsl:attribute name="id"><xsl:value-of select="cust_id"/></xsl:attribute>
            
            <name><xsl:value-of select="name"/></name>
            <address>
                <street><xsl:value-of select="street"/></street>
                <city><xsl:value-of select="city"/></city>
                <state><xsl:value-of select="state"/></state>
                <zipcode><xsl:value-of select="zipcode"/></zipcode>
            </address>
        </customer>
    </xsl:template>
</xsl:stylesheet>
